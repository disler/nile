# -*- coding: utf-8 -*-

#########################################################################
## This scaffolding model makes your app work on Google App Engine too
## File is released under public domain and you can use without limitations
#########################################################################

## if SSL/HTTPS is properly configured and you want all HTTP requests to
## be redirected to HTTPS, uncomment the line below:
# request.requires_https()

if not request.env.web2py_runtime_gae:
    ## if NOT running on Google App Engine use SQLite or other DB
    db = DAL('sqlite://storage.sqlite')
else:
    ## connect to Google BigTable (optional 'google:datastore://namespace')
    db = DAL('google:datastore+ndb')
    ## store sessions and tickets there
    session.connect(request, response, db=db)
    ## or store session in Memcache, Redis, etc.
    ## from gluon.contrib.memdb import MEMDB
    ## from google.appengine.api.memcache import Client
    ## session.connect(request, response, db = MEMDB(Client()))

## by default give a view/generic.extension to all actions from localhost
## none otherwise. a pattern can be 'controller/function.extension'
response.generic_patterns = ['*'] if request.is_local else []

## (optional) optimize handling of static files
# response.optimize_css = 'concat,minify,inline'
# response.optimize_js = 'concat,minify,inline'
## (optional) static assets folder versioning
# response.static_version = '0.0.0'
#########################################################################
## Here is sample code if you need for
## - email capabilities
## - authentication (registration, login, logout, ... )
## - authorization (role based authorization)
## - services (xml, csv, json, xmlrpc, jsonrpc, amf, rss)
## - old style crud actions
## (more options discussed in gluon/tools.py)
#########################################################################

from gluon.tools import Auth, Service, PluginManager

auth = Auth(db)
service = Service()
plugins = PluginManager()

## create all tables needed by auth if not custom tables
auth.define_tables(username=False, signature=False)

## configure email
mail = auth.settings.mailer
mail.settings.server = 'logging' if request.is_local else 'smtp.gmail.com:587'
mail.settings.sender = 'you@gmail.com'
mail.settings.login = 'username:password'

## configure auth policy
auth.settings.registration_requires_verification = False
auth.settings.registration_requires_approval = False
auth.settings.reset_password_requires_verification = True

## if you need to use OpenID, Facebook, MySpace, Twitter, Linkedin, etc.
## register with janrain.com, write your domain:api_key in private/janrain.key
from gluon.contrib.login_methods.janrain_account import use_janrain
use_janrain(auth, filename='private/janrain.key')

#########################################################################
## Define your tables below (or better in another model file) for example
##
## >>> db.define_table('mytable',Field('myfield','string'))
##
## Fields can be 'string','text','password','integer','double','boolean'
##       'date','time','datetime','blob','upload', 'reference TABLENAME'
## There is an implicit 'id integer autoincrement' field
## Consult manual for more options, validators, etc.
##
## More API examples for controllers:
##
## >>> db.mytable.insert(myfield='value')
## >>> rows=db(db.mytable.myfield=='value').select(db.mytable.ALL)
## >>> for row in rows: print row.id, row.myfield
#########################################################################

## after defining tables, uncomment below to enable auditing
# auth.enable_record_versioning(db)







#Accounts
db.define_table('account',
                Field('first_name' ), 
                Field('last_name' ), 
                Field('password', notnull=True),
                Field('email', notnull=True, unique=True), 
                Field('shipping_address'),
                Field('Answer'))

#shopping cart
db.define_table('cart',
                Field('account_id', 'reference account'),
                Field('product_ids', 'list:reference product'))

#placed_order
db.define_table('placed_order',
                Field('account_id', 'reference account'),
                Field('cart_id', 'reference cart'),
                Field('date_placed'),
                Field('delivery_date'))

#payment information
db.define_table('payment_information',
                Field('account_id', 'reference account'),
                Field('cc_number'),
                Field('exp_date'))

#products
db.define_table('product',
                Field('name', notnull=True),
                Field('producer', notnull=True),
                Field('upload_date'),
                Field('price', notnull=True),
                Field('description'),
                Field('occurrence'),
                Field('image', 'upload', default='../../static/images/skate.jpg'))

db.define_table('product_review',
                Field('account_id', 'reference account', notnull=True),
                Field('product_id', 'reference product'),
                Field('rating'),
                Field('review_description')
                )

import random
import time

mockNames = ['skateboard', 'toilet', 'rocket', 'trampoline', 'gum']
mockProducers = ['tegrat', 'ceiling-mart', 'costlyco', 'amalame', 'derpbook']
mockFirstNames = ['John', "Sammus", 'Mario', "Squall", "Cloud"]
mockLastNames = ['Snow', "Aran", 'Party', "Ragnorok", "Strife"]
mockEmails = ['test@test.com', 'test@test.com', 'test@test.com', 'test@test.com', 'test@test.com']
mockAddresses = ['31 N brook']

mockReviews = [ "This product is really legit, it broke before I even used it, how does that even happen.",
                "Best product ever, I think I'm done buying stuff now.",
                "Really solid product, literally.",
                "I actually wanted potatoes, but this did the trick",
                "This product is trash"]

#Database table insertions



#create accounts
if( len(db(db.account).select()) == 0):
    for i in xrange(10):
        mockFirstName = random.choice(mockFirstNames)
        mockLastName = random.choice(mockLastNames)
        mockEmail = random.choice(mockEmails)
        mockAddress = random.choice(mockAddresses)
        mockPassword = random.randrange(1000, 9999, 1)

        db.account.insert(  
                            first_name=mockFirstName,
                            last_name=mockLastName,
                            email=(mockEmail+str(mockPassword)),
                            shipping_address=mockAddress,
                            password=mockPassword
                            )


#create products
if( len(db(db.product).select()) == 0):
    #iterate
    for i in xrange(100):
        #create test data
        mockName = random.choice(mockNames)
        mockProducer = random.choice(mockProducers)
        mockPrice = random.randrange(10, 2000, 1)
        mockDescription = 'This product is really awesome, you should buy it because everyone loves spending money. When you have this product, you"ll get over it within minutes, and buy something new because marketing'
        mockOccurrence = random.randrange(1, 5, 1)

        #insert mock data into database
        db.product.insert   (  
                            name=mockName,
                            producer=mockProducer,
                            upload_date=(time.strftime("%d/%m/%Y")),
                            price=mockPrice,
                            occurrence=mockOccurrence,
                            description=mockDescription
                            )
'''
'''
#create reviews
if( len(db(db.product_review).select()) == 0 ):
    for i in xrange(200):
        mockRating = random.randrange(0, 5, 1)
        mockReviewDescription = random.choice(mockReviews)
        mockAccount = (random.choice(db(db.account).select())).id
        mockProduct = (random.choice(db(db.product).select())).id
        db.product_review.insert(   
                            account_id=mockAccount,
                            product_id=mockProduct,
                            rating=mockRating,
                            review_description=mockReviewDescription
                        )
