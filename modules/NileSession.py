
class NSession(object):

	def __init__(self):
		if !session['logged']:
			session['logged'] = False

	def add(self, key, value):
		session[key] = value

	def remove(self, key):
		del session[key]