def index():
	products=db(db.product).select()
	return dict(products=products)

def oldindex():


	#set up the 'logged' session
	if 'logged' not in session:
		session['logged'] = False

	logged = session['logged']

	# login form --------------------------------------------------------------------------- therealdezel@yahoo.com

	loginStatus = ''

	#do the email and password variables exist and are we logged off
	if request.vars.email and request.vars.password and session['logged'] == False:
		#ADD FIELD VALIDATION
		#validate(request.vars)

		#obtain email and password fields from post request
		email = request.vars.email
		password = request.vars.password
		#find the row in the db that contains our current email and passwrd
		emailAndPasswordRow = db(db.account.email == email and db.account.password == password).select()
		#determine the number of rows with these credentials (Should always be 1 or 0)
		doesAccountExists = len(emailAndPasswordRow)

		#if our password and email exists log in
		if doesAccountExists == 1:
			loginStatus = 'You have logged in'
			session['logged'] = True
			session['email'] = email

		else:
			loginStatus = 'You have entered the wrong email and password'
	
	# create account form --------------------------------------------------------------------------- 

	createAccountStatus = ''

	newAccount = request.vars
	#if all required fields are filled out, proceed
	if newAccount.firstNameNew and newAccount.lastNameNew and newAccount.emailNew and newAccount.passwordNew and newAccount.shippingAddressNew:
		#ADD FIELD VALIDATIOn
		#validate(request.vars)

		#insert fields into database
		db.account.insert(	first_name=newAccount.firstNameNew,
							last_name=newAccount.lastNameNew,
							email=newAccount.emailNew,
							password=newAccount.passwordNew,
							shipping_address=newAccount.shippingAddressNew
							)
		createAccountStatus = 'You have created a new account'
	else:
		createAccountStatus = 'Please enter all required fields'

	return dict(locals=locals(), loginStatus=loginStatus, createAccountStatus=createAccountStatus, logged=logged)

def landing():
	products=db(db.product).select()
	return dict(products=products)
