def index():
	productId = request.args[0]

	product = (db(db.product.id == productId).select())[0]

	reviews = db(db.product_review.product_id == productId).select()

	reviewRatingSum = 0
	totalReviews = len(reviews)
	averageRating = 0
	if totalReviews > 0:

		for review in reviews:
			reviewRatingSum = reviewRatingSum + int(review.rating)

		averageRating = reviewRatingSum / totalReviews

	return dict(product=product, reviews=reviews, rating=averageRating)